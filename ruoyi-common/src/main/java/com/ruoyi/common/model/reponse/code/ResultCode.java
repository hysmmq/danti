package com.ruoyi.common.model.reponse.code;

/**
 * 响应状态码的顶级父接口
 */
public interface ResultCode {

    /**
     * 操作状态码
     */
    int code();

    /**
     * 提示信息
     */
    String message();

}
