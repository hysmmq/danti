package com.ruoyi.common.model.reponse.result;

import com.ruoyi.common.model.reponse.code.ResultCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author: hysm
 * @Project: JavaLaity
 * @Pcakage: com.ruoyi.common.model.reponse.result.ResponseResult
 * @Date: 2023年07月20日 06:57
 * @Description: 全局通用返回结果
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("ResponseResult全局通用返回结果")
public class ResponseResult<T> implements Serializable {
    @ApiModelProperty("操作状态码")
    private Integer code;

    @ApiModelProperty("提示信息")
    private String message;

    @ApiModelProperty("返回结果数据")
    private T data;


    public ResponseResult(ResultCode resultCode) {
        this.code = resultCode.code();
        this.message = resultCode.message();
    }

    public ResponseResult(ResultCode resultCode, T data) {
        this.code = resultCode.code();
        this.message = resultCode.message();
        this.data = data;
    }
}