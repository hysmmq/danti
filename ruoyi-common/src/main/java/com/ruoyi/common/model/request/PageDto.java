package com.ruoyi.common.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author: hysm
 * @Project: JavaLaity
 * @Pcakage: com.ruoyi.common.model.request.PageDto
 * @Date: 2023年07月20日 06:47
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("PageDto分页请求参数")
public class PageDto implements Serializable {
    @NotNull(message = "当前页不能为空")
    @Min(value = 1, message = "当前页最小为1")
    @ApiModelProperty(value = "当前页", required = true)
    private Integer currPage;

    @NotNull(message = "每页显示数量不能为空")
    @Min(value = 1, message = "每页显示数量最小为1")
    @ApiModelProperty(value = "每页显示数量", required = true)
    private Integer pageSize;
}