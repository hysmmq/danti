package com.ruoyi.common.model.reponse.code;

import lombok.Getter;


/**
 * 通用的响应状态码
 */
@Getter
public enum CommonCode implements ResultCode{
    SUCCESS(200, "操作成功！"),
    FAIL(11111, "操作失败！"),
    INVALID_PARAM( 10001, "操作失败"),
    UNAUTHENTICATED( 10002, "请先登陆"),
    ACCESS_USER_STATUS_ERROR( 10003, "登录失败"),
    REPEAT_ERROR(10004, "您已操作成功,请勿重复操作！"),
    TOKEN_EXPIRE( 10005, "登录已过期，请重新登录"),
    ID_ERROR( 10006, "账号错误"),
    METHOD_NOT_SUPPORTED(10007, "操作失败"),
    NO_ACCESS( 10008, "无访问权限！"),
    OPERATION_IS_FREQUENT(10009, "操作太频繁了,请稍后重试！"),
    VERITY_CODE_ERROR(10012, "验证码错误，请输入正确的验证码！"),
    ACCESS_DENIED(10013, "拒绝访问！"),
    PHONE_ERROR(10014, "手机号码错误！"),
    VERITY_CODE_INVALID(10015, "验证码已失效,请重新获取！"),
    CHANNEL_ERROR(10016, "操作失败"),
    ADMIN_ERROR(10017, "超级管理员信息不允许修改! "),
    BIND_SUCCESS(10018, "当前微信已经绑定过手机号,请直接使用微信登录! "),
    ORDER_NOT_EXIST(10019, "当前订单号不存在,请刷新重试! "),
    ORDER_STATUS_NOT_2(10020, "当前订单号未支付成功,请刷新重试! "),
    ORDER_IS_COMMENT(10021, "当前订单已评价过,请勿重复评价! "),
    ORDER_IS_NOT_APPLICATION_INVOICE(10022, "当前订单未申请开票,请先申请开票! "),
    ORDER_STATUS_NOT_1(10023, "当前订单号不是待付款订单,请刷新重试! "),
    ORDER_STATUS_NOT_3(10024, "当前订单号不是已关闭订单,请刷新重试! "),
    WX_CREATE_ORDER_ERROR(10025, "创建订单失败"),
    WX_PAY_ERROR(10025, "支付失败"),
    PRE_PAY_ID_IS_EMPTY(10026, "订单失败 "),
    WX_PAY_CALLBACK_SIGN_ERROR(10027, "支付失败"),
    INVALID_NOTIFY_PARAM(10028, "支付失败"),
    WX_PAY_QUERY_ORDER_ERROR(10028, "未查询到订单 "),
    ALI_PAY_ERROR(10029, "支付失败"),
    ALI_PAY_CALLBACK_ERROR(10030, "支付失败"),
    PLATFORM_ERROR(10031, "操作失败"),
    REPEAT_BUY(10031, "该类型服务包已购买，请勿重复购买"),
    REPEAT_SUBMIT(10032, "该类型的服务包订单已存在。如您要重新下单，须先前往“我的服务”取消未支付的订单"),


    AUTH_LOGIN_TOKEN_SAVE_FAIL(23001,"登录已过期，请重新登录"),
    AUTH_LOGIN_APPLY_TOKEN_FAIL(23002,"验证登录失败！"),
    AUTH_CREDENTIAL_ERROR(23003,"账号或密码错误！"),
    ACCOUNT_NOT_EXIST(23004,"账号不存在！"),
    PHONE_OR_CODE_ERROR(23005,"手机号或验证码错误！"),
    PHONE_NOT_EXIST(23006,"手机号未注册！"),
    CAN_NOT_GET_CODE(23007,"X秒后可重新获取验证码！"),
    PASSWORD_ERROR(23008,"密码(验证码)错误"),
    QR_CODE_IS_INVALID(23009,"二维码已过期，请刷新重试"),
    SERVICE_PACKAGE_IS_NOT_EXIST(23010,"服务包不存在，请刷新重试"),
    NO_SUCH_INTEREST(23011,"暂无该权益"),
    AGE_IS_INVALID(23011,"您的年龄已超出本运动计划的服务范围"),
    SMS_LIMIT(23012,"今日验证码获取已超限，请明日重试"),
    NOT_START_SPORT_PLAN(23013,"尚未开启运动计划"),
    AGE_INVALID(23014,"您的年龄已超出本运动计划的服务范围"),


    USERCODE_NEW_FAIL( 30001, "获取最新用户工号失败！"),
    USENAME_REPEAT( 30002, "用户名已经存在！"),
    STAFFINFO_ISEMPTY( 30003, "员工信息为空！"),
    STAFFINFO_NOT_EXIST( 30004, "员工信息不存在！"),
    FROMTSYSTEMIDS_NOT_EXIST( 30005, "有crm前端系统id不存在！"),
    SERVESYSTEMIDS_NOT_EXIST( 30006, "有crm后端系统id不存在！"),
    USENAME_CHECK_PASSWORD_ERROR( 30007, "密码校验不通过！"),
    CODE_ERROR(30008, "截取编码不符合要求！"),
    USENAME_NOT_EXIST( 30009, "用户信息不存在！"),
    ROLE_NOT_EXIST( 30010, "角色信息不存在！"),
    ROLENAME_REPEAT( 30011, "同应用下角色名称不可重复！"),
    APPACTION_NOT_EXIST( 30012, "选择应用不存在！"),
    ROLE_KEY_NOT_REPEAT( 30013, "权限字符不能重复！"),
    MENUTYPE_ERROR( 30014, "菜单类型错误！"),
    UPDATE_DATE_NOT_EXIST( 30015, "修改数据不存在！"),
    PHONE_STAFF_NOT_MATCH( 30016, "手机号与员工信息不匹配！"),

    DEPTNAME_REPEAT( 40001, "部门名已经存在！"),
    DEPT_NOT_FIND( 40002, "部门不存在！"),
    PPSTNAME_REPEAT( 40011, "岗位名已经存在！"),
    POST_MUST( 40012, "岗位信息不能为空！"),
    STAFFNAME_REPEAT( 40021, "员工姓名已经存在！"),
    STAFF_NOT_REPEAT( 40022, "员工不存在！"),
    STAFFTEL_REPEAT( 40023, "员工手机号已经存在！"),
    STAFFPOST_RELATION( 40031, "员工岗位关系不存在！"),
    STAFFPOST_EXIST( 40032, "员工岗位关系已存在，无需重复创建！"),




    MSG_IMG_SEC_CHECK_ERR(87014, "含有违法违规内容！"),
    SERVER_BUSY(55555, "系统繁忙，请稍后重试！"),
    SERVER_ERROR( 99999, "抱歉，系统异常，请稍后重试！");

    /**
     * 操作代码
     */
    int code;
    /**
     * 提示信息
     */
    String message;

    CommonCode(int code, String message) {

        this.code = code;
        this.message = message;
    }


    @Override
    public int code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
