package com.ruoyi.common.model.reponse.page;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author: hysm
 * @Project: JavaLaity
 * @Pcakage: com.ruoyi.common.model.reponse.page.PageResult
 * @Date: 2023年07月20日 06:54
 * @Description: 通用的查询结果响应体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "pageResult分页对象", description = "分页统一对象")
public class PageResult<T> implements Serializable {

    @ApiModelProperty("数据列表")
    private List<T> list;

    @ApiModelProperty("总记录数")
    private Long totalCount;

    @ApiModelProperty("每页记录数")
    private Long pageSize;

    @ApiModelProperty("总页数")
    private Long totalPage;

    @ApiModelProperty("当前页数")
    private Long currPage;

    public PageResult(List<T> list, Long totalCount, Long pageSize, Long currPage) {
        this.list = list;
        this.totalCount = totalCount;
        this.pageSize = pageSize;
        this.currPage = currPage;
        this.totalPage = (long)Math.ceil((double)totalCount / (double)pageSize);
    }

    public PageResult(IPage<T> page) {
        this.list = page.getRecords();
        this.totalCount = page.getTotal();
        this.pageSize = page.getSize();
        this.currPage = page.getCurrent();
        this.totalPage = page.getPages();
    }

}